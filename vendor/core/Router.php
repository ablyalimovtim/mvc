<?php

namespace vendor\core;

class Router 
{
	private static $route = [];
	private static $routes = [];

	public static function add($regexp, $route = [])
	{
		self::$routes[$regexp] = $route;
	}

	public static function matchRoute($uri)
	{ 
		foreach (self::$routes as $pattern => $route) { 
			if (preg_match("#$pattern#i", $uri, $matches)) { 
				foreach ($matches as $k => $v) { 
					if (is_string($k)) {
						$route[$k] = $v;
					}
				}

				if (!isset($route['action'])) {
					$route['action'] = 'index';
				}

				self::$route = $route; 

				return true;
			}	
		}

		return false;
	}

	public static function dispatch($uri) 
	{ 
		if (self::matchRoute($uri)) { 
			$controller = 'app\controllers\\' . ucfirst(self::$route['controller']) . 'Controller'; 

			if (class_exists($controller)) { 
				$obj = new $controller();
				$action = self::$route['action']; 

				if (method_exists($obj, $action)) {
					(!empty(self::$route['param'])) ? $obj->$action(self::$route['param']) : $obj->$action();
				} else {
					echo 'Метод ' . $action . ' не найден!';
				}	
			} else {
				echo 'Контроллер ' . $controller . ' не найден!';
			}
		} else {
			die ('404 Not Found');
		}		
	}
}	