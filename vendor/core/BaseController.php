<?php

namespace vendor\core;

abstract class BaseController
{

	public function view($view, $template, $data = [])
	{ 
		require_once('../app/views/' . $template . '.html');
	}
}