<?php

namespace app\controllers;

use vendor\core\BaseController;
use app\model\Task;

class TaskController extends BaseController
{
	public function show($id)
	{  
		$task = (new Task)->prepare('SELECT * FROM task WHERE id = :id'); 
		$task->bindParam(':id', $id);
		$task->execute();	 

		$this->view('task/show', 'template', $task);
	}

	public function index() 
	{ 		
		if (!isset($_GET['page']) || isset($_GET['page']) && $_GET['page'] == 1) {
			$page = 0;
		} else {
			$page = ($_GET['page'] * 3) - 3;
		} 

		$field = 'name';
		$order = 'desc';

		if (isset($_GET['sort'])) { 
			$field = $_GET['sort']['field'];
			$order = $_GET['sort']['order'];
			
			if ($order == 'desc') {
				$order = 'asc';
			} else {
				$order = 'desc';
			}
		} 

		$tasks = (new Task)->query("SELECT * FROM task ORDER BY $field $order LIMIT $page, 3");

		$countPage = (new Task)->query('SELECT * FROM task')->rowCount();

		$this->view('task/index', 'template', [
			'tasks' => $tasks,
			'countPage' => $countPage,
			'field' => $field,
			'order' => $order,
		]);	
	}
	
	public function create()
	{ 
		$this->view('task/create', 'template');
	}

	public function save()
	{ 
		if ($_SERVER['REQUEST_METHOD'] == 'POST') { 
			$allowedExtensions = ['jpg', 'jpeg', 'png']; 
			$imageExtansion = $_FILES['image']['type'];
			$imageExtansion = preg_replace('#image\/#', '', $imageExtansion); 


			if (!empty($_FILES && in_array($imageExtansion, $allowedExtensions))) {
				$newWidth = 320;
				$newHeight = 240; 

				$imageName = basename($_FILES['image']['name']); 
				$tmpName = $_FILES['image']['tmp_name'];
				$uploadsDir = dirname(__DIR__) . '/../public/images/' . $imageName;

				
				list($oldWidth, $oldHeight) = getimagesize($tmpName); 
			
				$func = 'imagecreatefrom' . $imageExtansion;
				$srcResource = $func($tmpName);

				$destinationResourse = imagecreatetruecolor($newWidth, $newHeight);

				imagecopyresampled($destinationResourse, $srcResource, 0, 0, 0, 0, $newWidth, $newHeight, $oldWidth, $oldHeight);

				$func = 'image' . $imageExtansion; 
				$func($destinationResourse, $uploadsDir);
			}	

			$task = (new Task)->prepare("INSERT INTO task(name, image, content, done)
				VALUES(:name, :image, :content, :done)"
			); 

			$done = (isset($_POST['done'])) ? 1 : 0;
			$image = (isset($_FILES)) ? $_FILES['image']['name'] : null;

			$task->bindParam(':name', $_POST['name']);
			$task->bindParam(':image', $image);
			$task->bindParam(':content', $_POST['content']);
			$task->bindParam(':done', $done);

			$task->execute();

			header("Location: /task");
			
		}
	}

	public function edit($id)
	{
		$task = (new Task)->query("SELECT * FROM task WHERE id = $id");

		$this->view('task/edit', 'template', $task->fetchAll());
	}

	public function update($id)
	{ 
		if ($_SERVER['REQUEST_METHOD'] == 'POST') { 
			$name = $_POST['name'];
			$content = $_POST['content'];
			$done = (isset($_POST['done'])) ? 1 : 0;
			$image = (isset($_FILES)) ? $_FILES['image']['name'] : null;

			$task = (new Task)->prepare(
				"UPDATE task
				SET name = :name, content = :content, done = :done, image = :image
				WHERE id = :id" 
			);  

			$task->bindParam(':name', $_POST['name']);
			$task->bindParam(':content', $_POST['content']);
			$task->bindParam(':done', $done);
			$task->bindParam(':image', $image);			
			$task->bindParam(':id', $id, Task::PARAM_INT);

			$task->execute(); 

			header("Location: /task/show/$id");
		}
	}

	public function delete($id)
	{
		$task = (new Task)->query("DELETE from task WHERE id = $id");

		header("Location: /task");
	}
}