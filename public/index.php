<?php

ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

use vendor\core\Router;

$uri = trim($_SERVER['REQUEST_URI'], '/'); 

spl_autoload_register(function($class) {
	$file = dirname(__DIR__) . '/' . str_replace('\\', '/', $class) . '.php';  
	
	if (file_exists($file)) {
		include($file); 
	}
});

Router::add('^$', ['controller' => 'Task', 'action' =>'index']);
Router::add('(?P<controller>[a-z-]+)/?(?P<action>[a-z-]+)?/?(?P<param>[0-9])?');

Router::dispatch($uri);

